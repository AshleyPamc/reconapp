﻿
namespace MEMBER_RECON
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHpcode = new System.Windows.Forms.Label();
            this.cbxHpCode = new System.Windows.Forms.ComboBox();
            this.lblFilePath = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.cbxType = new System.Windows.Forms.ComboBox();
            this.btnFile = new System.Windows.Forms.Button();
            this.btnRecon = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxMonth = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHpcode
            // 
            this.lblHpcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHpcode.Location = new System.Drawing.Point(32, 80);
            this.lblHpcode.Name = "lblHpcode";
            this.lblHpcode.Size = new System.Drawing.Size(100, 23);
            this.lblHpcode.TabIndex = 0;
            this.lblHpcode.Text = "Healthplan";
            // 
            // cbxHpCode
            // 
            this.cbxHpCode.FormattingEnabled = true;
            this.cbxHpCode.Items.AddRange(new object[] {
            "Affinity Health",
            "Affinity Dental",
            "NBCRLFI",
            "NBCPSS",
            "Hosmed",
            "Wesmart",
            "Getsavvi",
            "MediCall",
            "Profmed",
            "AGS",
            "Asterio",
            "Acumen",
            "Motohealth",
            "Sizwe",
            "Essentialmed"});
            this.cbxHpCode.Location = new System.Drawing.Point(151, 80);
            this.cbxHpCode.Name = "cbxHpCode";
            this.cbxHpCode.Size = new System.Drawing.Size(303, 21);
            this.cbxHpCode.TabIndex = 1;
            // 
            // lblFilePath
            // 
            this.lblFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilePath.Location = new System.Drawing.Point(45, 22);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(87, 23);
            this.lblFilePath.TabIndex = 2;
            this.lblFilePath.Text = "File Path";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(151, 24);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(303, 20);
            this.textBox1.TabIndex = 3;
            // 
            // lblType
            // 
            this.lblType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblType.Location = new System.Drawing.Point(27, 191);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(105, 23);
            this.lblType.TabIndex = 4;
            this.lblType.Text = "Recon Type";
            // 
            // cbxType
            // 
            this.cbxType.FormattingEnabled = true;
            this.cbxType.Items.AddRange(new object[] {
            "main members only",
            "all members"});
            this.cbxType.Location = new System.Drawing.Point(151, 192);
            this.cbxType.Name = "cbxType";
            this.cbxType.Size = new System.Drawing.Size(303, 21);
            this.cbxType.TabIndex = 5;
            // 
            // btnFile
            // 
            this.btnFile.Location = new System.Drawing.Point(472, 22);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(97, 22);
            this.btnFile.TabIndex = 6;
            this.btnFile.Text = "Select Folder";
            this.btnFile.UseVisualStyleBackColor = true;
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // btnRecon
            // 
            this.btnRecon.Location = new System.Drawing.Point(49, 3);
            this.btnRecon.Name = "btnRecon";
            this.btnRecon.Size = new System.Drawing.Size(75, 23);
            this.btnRecon.TabIndex = 7;
            this.btnRecon.Text = "Start Recon";
            this.btnRecon.UseVisualStyleBackColor = true;
            this.btnRecon.Click += new System.EventHandler(this.btnRecon_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(172, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Stop Recon";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(235, 27);
            this.label1.TabIndex = 9;
            // 
            // cbxMonth
            // 
            this.cbxMonth.FormattingEnabled = true;
            this.cbxMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cbxMonth.Location = new System.Drawing.Point(151, 140);
            this.cbxMonth.Name = "cbxMonth";
            this.cbxMonth.Size = new System.Drawing.Size(303, 21);
            this.cbxMonth.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 23);
            this.label2.TabIndex = 10;
            this.label2.Text = "Recon Month";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblFilePath);
            this.panel1.Controls.Add(this.cbxMonth);
            this.panel1.Controls.Add(this.lblHpcode);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbxHpCode);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.lblType);
            this.panel1.Controls.Add(this.cbxType);
            this.panel1.Controls.Add(this.btnFile);
            this.panel1.Location = new System.Drawing.Point(21, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(602, 239);
            this.panel1.TabIndex = 12;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnRecon);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Location = new System.Drawing.Point(160, 258);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(315, 35);
            this.panel2.TabIndex = 13;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(172, 311);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(299, 66);
            this.panel3.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 389);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Member Recon App";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblHpcode;
        private System.Windows.Forms.ComboBox cbxHpCode;
        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.ComboBox cbxType;
        private System.Windows.Forms.Button btnFile;
        private System.Windows.Forms.Button btnRecon;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxMonth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
    }
}

