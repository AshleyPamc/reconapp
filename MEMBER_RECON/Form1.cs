﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MEMBER_RECON
{
    public partial class Form1 : Form
    {

        private string _hpCode = "";
        private string _fileId = "";
        private string _fileCount = "";
        private bool _reconType = false;
        private string _rlshipMain = "";
        private string _healthplanName = "";
        private string _dateToRecon = "";
        private DateTime _date = DateTime.Now;




        public string _outputDirectory = "";

        public string HpCode { get => _hpCode; set => _hpCode = value; }
        public string FileID { get => _fileId; set => _fileId = value; }
        public string FileCount { get => _fileCount; set => _fileCount = value; }
        public bool ReconType { get => _reconType; set => _reconType = value; }
        public string RlshipMain { get => _rlshipMain; set => _rlshipMain = value; }

        public Form1()
        {
            
            InitializeComponent();
        }

        public void StartProcessing()
        {
            try
            {

                
               

                panel1.Enabled = false;
              //  panel3.Visible = true;
                label1.Text = "Generating Recon File....";
                

                decimal systemCount = 0;
                decimal chfCount = 0;
                decimal droppedCount = 0;
                decimal errorCount = 0;

                DataTable dtChf = new DataTable();
                dtChf.Columns.Add("DESCRIPTION");
                dtChf.Columns.Add("SHEET");
                dtChf.Columns.Add("COUNT");
                DataTable dtSystem = new DataTable();
                dtSystem.Columns.Add("DESCRIPTION");
                dtSystem.Columns.Add("SHEET");
                dtSystem.Columns.Add("COUNT");

                SqlConnection cn = new SqlConnection("Server=192.168.16.13;Database=DRC;User Id=sa;Password = galnetdata; Trusted_Connection=false ");
                SqlCommand cmd = new SqlCommand();
                List<DataTable> list = new List<DataTable>();

                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.CommandTimeout = 6000;

                cmd.Connection = cn;

                if (!_reconType)
                {
                    cmd.Parameters.Add(new SqlParameter("@FILEID", _fileId));
                    cmd.Parameters.Add(new SqlParameter("@DATERECON", _dateToRecon));

                    cmd.Parameters.Add(new SqlParameter("@FILECOUNT", _fileCount));
                    cmd.CommandText = "EXEC dbo.EDI_MEMBERFILE_RECONS @FILEID,@FILECOUNT,@DATERECON";
                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    dt.TableName = $"CHF_{DateTime.Now.Year}{DateTime.Now.Month}01";

                    DataRow[] result = dt.Select("ACTUAL = 'SUSPENDED BUT ACTIVE FOR THIS MONTH' OR ACTUAL = 'ACTIVE'");
                    chfCount = result.Length;

                    list.Add(dt);

                    cmd.Parameters.Add(new SqlParameter("@HPCODE", _hpCode));
                    cmd.CommandText = "EXEC dbo.EDI_MEMBER_RECON_SYSTEM_CNT @HPCODE,@DATERECON";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    dt.TableName = "DRC_PAMC_SYSTEM";

                    systemCount = dt.Rows.Count;

                    list.Add(dt);

                    decimal percentDif = 0;

                    if (systemCount > chfCount)
                    {

                        percentDif = Math.Round(100 - Math.Round((chfCount / systemCount) * 100, 2), 1);

                        if (percentDif > 2)
                        {
                            cmd.CommandText = "EXEC dbo.EDI_MEMBER_RECON_DROPPED @FILEID,@FILECOUNT,@HPCODE,@DATERECON";
                            dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());
                            dt.TableName = "DROPPED_MEMBERS";
                            droppedCount = dt.Rows.Count;
                            list.Add(dt);

                            percentDif = 100 - Math.Round(((chfCount + droppedCount) / systemCount) * 100, 2);

                            if (percentDif > 5 || droppedCount == 0)
                            {
                                cmd.CommandText = "EXEC dbo.EDI_MEMBER_RECON_ERROR @FILEID,@FILECOUNT,@HPCODE,@DATERECON";
                                dt = new DataTable();
                                dt.Load(cmd.ExecuteReader());
                                dt.TableName = "ERROR_MEMBERS";
                                errorCount = dt.Rows.Count;
                                list.Add(dt);

                                percentDif = 100 - Math.Round(((chfCount + droppedCount) / (systemCount - errorCount)) * 100, 2);

                            }

                            DataTable finale = new DataTable();
                            finale.Columns.Add("DESCRIPTION");
                            finale.Columns.Add("SHEET");
                            finale.Columns.Add("COUNT");








                            object[] chf = { "ACTIVE MEMBERS ON CHF", $"CHF_{DateTime.Now.Year}{DateTime.Now.Month}01", $"{chfCount}" };
                            dtChf.Rows.Add(chf);

                            object[] sys = { "ACTIVE MEMBERS ON DRC/PAMC SYSTEM", "DRC_PAMC_SYSTEM", $"{systemCount}" };
                            dtSystem.Rows.Add(sys);

                            if (droppedCount > 0)
                            {
                                object[] drop = { "MEMBERS DROPPED FROM CHF THAT ARE STILL ACTIVE", "DROPPED_MEMBERS", $"{droppedCount}" };
                                dtSystem.Rows.Add(drop);
                            }

                            if (errorCount > 0)
                            {
                                object[] err = { "MEMBERS THAT HAD ERRORS THAT ARE STILL ACTIVE", "ERROR_MEMBERS", $"{errorCount}" };
                                dtSystem.Rows.Add(err);
                            }

                            //finale.TableName = "SUMMARY";
                            //FileInfo file = new FileInfo($"{base._outputDirectory}/{HpCode}_RECON_{DateTime.Now.ToString("yyyyMMdd")}.xlsx");

                            object[] totalCHF = { "", $"TOTAL", $"{chfCount }" };
                            dtChf.Rows.Add(totalCHF);

                            object[] totalSYS = { "", "TOTAL", $"{systemCount - errorCount - droppedCount}" };
                            dtSystem.Rows.Add(totalSYS);
                            //list.Reverse();



                        }
                        else
                        {
                            object[] chf = { "ACTIVE MEMBERS ON CHF", $"CHF_{DateTime.Now.Year}{DateTime.Now.Month}01", $"{chfCount}" };
                            dtChf.Rows.Add(chf);

                            object[] sys = { "ACTIVE MEMBERS ON DRC/PAMC SYSTEM", "DRC_PAMC_SYSTEM", $"{systemCount}" };
                            dtSystem.Rows.Add(sys);

                            if (droppedCount > 0)
                            {
                                object[] drop = { "MEMBERS DROPPED FROM CHF THAT ARE STILL ACTIVE", "DROPPED_MEMBERS", $"{droppedCount}" };
                                dtSystem.Rows.Add(drop);
                            }

                            if (errorCount > 0)
                            {
                                object[] err = { "MEMBERS THAT HAD ERRORS THAT ARE STILL ACTIVE", "ERROR_MEMBERS", $"{errorCount}" };
                                dtSystem.Rows.Add(err);
                            }

                            //finale.TableName = "SUMMARY";
                            //FileInfo file = new FileInfo($"{base._outputDirectory}/{HpCode}_RECON_{DateTime.Now.ToString("yyyyMMdd")}.xlsx");

                            object[] totalCHF = { "", $"TOTAL", $"{chfCount}" };
                            dtChf.Rows.Add(totalCHF);

                            object[] totalSYS = { "", "TOTAL", $"{systemCount - errorCount - droppedCount}" };
                            dtSystem.Rows.Add(totalSYS);
                            //list.Reverse();



                        }
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();

                        FileInfo f = new FileInfo($"{_outputDirectory}/{HpCode}_RECON_{DateTime.Now.ToString("yyyyMMdd")}.xlsx");
                        list.Reverse();
                        PAMC.ExcelUtil.ExcelReport excel = new PAMC.ExcelUtil.ExcelReport(f, list, true, true);
                        excel.CreateReport();

                        ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial;
                        using (ExcelPackage package = new ExcelPackage(f))
                        {
                            ExcelWorksheet wss = package.Workbook.Worksheets.Add("SUMMARY");

                            for (int i = 0; i < dtChf.Rows.Count; i++)
                            {
                                for (int j = 2; j < dtChf.Columns.Count + 2; j++)
                                {
                                    if (i == 0)
                                    {
                                        wss.Cells[i + 2, j].Style.Font.Size = 12;
                                        wss.Cells[i + 2, j].Style.Font.Bold = true;
                                        wss.Cells[i + 2, j].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wss.Cells[i + 2, j].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.CornflowerBlue);
                                        wss.Cells[i + 2, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                        wss.Cells[i + 2, j].Value = dtChf.Columns[j - 2].ColumnName.ToString();
                                        wss.Cells[i + 3, j].Value = dtChf.Rows[i][j - 2].ToString();
                                        wss.Cells[i + 3, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                    }
                                    else
                                    {
                                        wss.Cells[i + 3, j].Value = dtChf.Rows[i][j - 2].ToString();
                                        wss.Cells[i + 3, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                    }
                                }
                            }


                            for (int i = 0; i < dtSystem.Rows.Count; i++)
                            {
                                for (int j = 2; j < dtSystem.Columns.Count + 2; j++)
                                {
                                    if (i == 0)
                                    {
                                        wss.Cells[i + 2, j + 8].Value = dtSystem.Columns[j - 2].ColumnName.ToString();
                                        wss.Cells[i + 2, j + 8].Style.Font.Size = 12;
                                        wss.Cells[i + 2, j + 8].Style.Font.Bold = true;
                                        wss.Cells[i + 2, j + 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wss.Cells[i + 2, j + 8].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.CornflowerBlue);
                                        wss.Cells[i + 2, j + 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                        wss.Cells[i + 3, j + 8].Value = dtSystem.Rows[i][j - 2].ToString();
                                        wss.Cells[i + 3, j + 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                    }
                                    else
                                    {
                                        wss.Cells[i + 3, j + 8].Value = dtSystem.Rows[i][j - 2].ToString();
                                        wss.Cells[i + 3, j + 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                                    }
                                }
                            }

                            wss.Columns.AutoFit();
                            wss.Cells[1, 2, 1, 4].Merge = true;
                            wss.Cells[1, 2, 1, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            wss.Cells[1, 2].Value = "CARDHOLDER FILE";
                            wss.Cells[1, 2].Style.Font.Bold = true;

                            wss.Cells[1, 10, 1, 12].Merge = true;
                            wss.Cells[1, 10, 1, 12].Style.HorizontalAlignment  = ExcelHorizontalAlignment.Center;
                            wss.Cells[1, 10].Value = "CPS SYSTEM";
                            wss.Cells[1, 10].Style.Font.Bold = true;
                            package.Save();

                        }

                    }
                    else
                    {

                    }
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@DATERECON", _dateToRecon));
                    cmd.Parameters.Add(new SqlParameter("@FILEID", _fileId));
                    cmd.Parameters.Add(new SqlParameter("@FILECOUNT", _fileCount));
                    cmd.CommandText = "EXEC dbo.EDI_MEMBERFILE_RECONS @FILEID,@FILECOUNT,@DATERECON";
                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    dt.TableName = $"CHF_{DateTime.Now.Year}{DateTime.Now.Month}01";

                    if(_rlshipMain != "00")
                    {
                        DataRow[] result = dt.Select($"RLSHIP = '{_rlshipMain}' AND (ACTUAL = 'SUSPENDED BUT ACTIVE FOR THIS MONTH' OR ACTUAL = 'ACTIVE')");
                        chfCount = result.Length;
                    }
                    else
                    {
                        DataRow[] result = dt.Select($"(RLSHIP = '{_rlshipMain}' OR RLSHIP = '0') AND (ACTUAL = 'SUSPENDED BUT ACTIVE FOR THIS MONTH' OR ACTUAL = 'ACTIVE')");
                        chfCount = result.Length;
                        if(chfCount == 0)
                        {
                            DataRow[] results = dt.Select($"(RLSHIP = '01' OR RLSHIP = '1') AND (ACTUAL = 'SUSPENDED BUT ACTIVE FOR THIS MONTH' OR ACTUAL = 'ACTIVE')");
                            chfCount = results.Length;
                        }
                    }
                    

                    list.Add(dt);

                    cmd.Parameters.Add(new SqlParameter("@HPCODE", _hpCode));
                    cmd.CommandText = "EXEC dbo.EDI_MEMBER_RECON_SYSTEM_CNT @HPCODE,@DATERECON";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    dt.TableName = "DRC_PAMC_SYSTEM";


                    DataTable d = new DataTable();
                    d = dt.Select("RLSHIP = '1'").CopyToDataTable();

                    systemCount = d.Rows.Count;

                    list.Add(d);

                    decimal percentDif = 0;

                    if (systemCount > chfCount)
                    {

                        percentDif = Math.Round(100 - Math.Round((chfCount / systemCount) * 100, 2), 1);

                        if (percentDif > 2)
                        {
                            cmd.CommandText = "EXEC dbo.EDI_MEMBER_RECON_DROPPED @FILEID,@FILECOUNT,@HPCODE,@DATERECON";
                            dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());
                            dt.TableName = "DROPPED_MEMBERS";
                            
                            

                              d = new DataTable();
                            if (_rlshipMain != "00")
                            {
                                d = dt.Select($"RLSHIP = '{_rlshipMain}'").CopyToDataTable();
                                droppedCount = d.Rows.Count;
                            }
                            else
                            {
                                DataRow[] result = dt.Select($"(RLSHIP = '{_rlshipMain}' OR RLSHIP = '0')");
                                droppedCount = result.Length;
                                if (droppedCount == 0)
                                {
                                    DataRow[] results = dt.Select($"(RLSHIP = '01' OR RLSHIP = '1')");
                                    droppedCount = results.Length;
                                    d = dt.Select($"(RLSHIP = '01' OR RLSHIP = '1')").CopyToDataTable();
                                }
                                else
                                {
                                    d = dt.Select($"(RLSHIP = '{_rlshipMain}' OR RLSHIP = '0')").CopyToDataTable();
                                }
                            }
                            list.Add(d);

                            percentDif = 100 - Math.Round(((chfCount + droppedCount) / systemCount) * 100, 2);

                            if (percentDif > 5 || droppedCount == 0)
                            {
                                cmd.CommandText = "EXEC dbo.EDI_MEMBER_RECON_ERROR @FILEID,@FILECOUNT,@HPCODE,@DATERECON";
                                dt = new DataTable();
                                dt.Load(cmd.ExecuteReader());
                                dt.TableName = "ERROR_MEMBERS";
                                

                                if (_rlshipMain != "00")
                                {
                                    d = dt.Select($"RLSHIP = '{_rlshipMain}'").CopyToDataTable();
                                    errorCount = d.Rows.Count;
                                }
                                else
                                {
                                    DataRow[] result = dt.Select($"(RLSHIP = '{_rlshipMain}' OR RLSHIP = '0')");
                                    errorCount = result.Length;
                                    if (errorCount == 0)
                                    {
                                        DataRow[] results = dt.Select($"(RLSHIP = '01' OR RLSHIP = '1')");
                                        errorCount = results.Length;
                                        d = dt.Select($"(RLSHIP = '01' OR RLSHIP = '1')").CopyToDataTable();
                                    }
                                    else
                                    {
                                        d = dt.Select($"(RLSHIP = '{_rlshipMain}' OR RLSHIP = '0')").CopyToDataTable();
                                    }
                                }

                                list.Add(d);

                                percentDif = 100 - Math.Round(((chfCount + droppedCount) / (systemCount - errorCount)) * 100, 2);

                            }
                            DataTable finale = new DataTable();
                            finale.Columns.Add("DESCRIPTION");
                            finale.Columns.Add("SHEET");
                            finale.Columns.Add("COUNT");








                            object[] chf = { "ACTIVE MEMBERS ON CHF", $"CHF_{DateTime.Now.Year}{DateTime.Now.Month}01", $"{chfCount}" };
                            dtChf.Rows.Add(chf);

                            object[] sys = { "ACTIVE MEMBERS ON DRC/PAMC SYSTEM", "DRC_PAMC_SYSTEM", $"{systemCount}" };
                            dtSystem.Rows.Add(sys);

                            if (droppedCount > 0)
                            {
                                object[] drop = { "MEMBERS DROPPED FROM CHF THAT ARE STILL ACTIVE", "DROPPED_MEMBERS", $"{droppedCount}" };
                                dtSystem.Rows.Add(drop);
                            }

                            if (errorCount > 0)
                            {
                                object[] err = { "MEMBERS THAT HAD ERRORS THAT ARE STILL ACTIVE", "ERROR_MEMBERS", $"{errorCount}" };
                                dtSystem.Rows.Add(err);
                            }

                            //finale.TableName = "SUMMARY";
                            //FileInfo file = new FileInfo($"{base._outputDirectory}/{HpCode}_RECON_{DateTime.Now.ToString("yyyyMMdd")}.xlsx");

                            object[] totalCHF = { "", $"TOTAL", $"{chfCount}" };
                            dtChf.Rows.Add(totalCHF);

                            object[] totalSYS = { "", "TOTAL", $"{systemCount - errorCount - droppedCount}" };
                            dtSystem.Rows.Add(totalSYS);
                            //list.Reverse();



                        }
                        else
                        {
                            object[] chf = { "ACTIVE MEMBERS ON CHF", $"CHF_{DateTime.Now.Year}{DateTime.Now.Month}01", $"{chfCount}" };
                            dtChf.Rows.Add(chf);

                            object[] sys = { "ACTIVE MEMBERS ON DRC/PAMC SYSTEM", "DRC_PAMC_SYSTEM", $"{systemCount}" };
                            dtSystem.Rows.Add(sys);

                            if (droppedCount > 0)
                            {
                                object[] drop = { "MEMBERS DROPPED FROM CHF THAT ARE STILL ACTIVE", "DROPPED_MEMBERS", $"{droppedCount}" };
                                dtSystem.Rows.Add(drop);
                            }

                            if (errorCount > 0)
                            {
                                object[] err = { "MEMBERS THAT HAD ERRORS THAT ARE STILL ACTIVE", "ERROR_MEMBERS", $"{errorCount}" };
                                dtSystem.Rows.Add(err);
                            }

                            //finale.TableName = "SUMMARY";
                            //FileInfo file = new FileInfo($"{base._outputDirectory}/{HpCode}_RECON_{DateTime.Now.ToString("yyyyMMdd")}.xlsx");

                            object[] totalCHF = { "", $"TOTAL", $"{chfCount}" };
                            dtChf.Rows.Add(totalCHF);

                            object[] totalSYS = { "", "TOTAL", $"{systemCount - errorCount - droppedCount}" };
                            dtSystem.Rows.Add(totalSYS);
                            //list.Reverse();



                        }
                        cn.Close();
                        cn.Dispose();
                        cmd.Dispose();
                        FileInfo f = new FileInfo($"{_outputDirectory}/{HpCode}_RECON_{DateTime.Now.ToString("yyyyMMdd")}.xlsx");
                        list.Reverse();
                        PAMC.ExcelUtil.ExcelReport excel = new PAMC.ExcelUtil.ExcelReport(f, list, true, true);
                        excel.CreateReport();

                        ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial;
                        using (ExcelPackage package = new ExcelPackage(f))
                        {
                            ExcelWorksheet wss = package.Workbook.Worksheets.Add("SUMMARY");

                            for (int i = 0; i < dtChf.Rows.Count; i++)
                            {
                                for (int j = 2; j < dtChf.Columns.Count + 2; j++)
                                {
                                    if (i == 0)
                                    {
                                        wss.Cells[i + 2, j].Style.Font.Size = 12;
                                        wss.Cells[i + 2, j].Style.Font.Bold = true;
                                        wss.Cells[i + 2, j].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wss.Cells[i + 2, j].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.CornflowerBlue);
                                        wss.Cells[i + 2, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                        wss.Cells[i + 2, j].Value = dtChf.Columns[j - 2].ColumnName.ToString();
                                        wss.Cells[i + 3, j].Value = dtChf.Rows[i][j - 2].ToString();
                                        wss.Cells[i + 3, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                    }
                                    else
                                    {
                                        wss.Cells[i + 3, j].Value = dtChf.Rows[i][j - 2].ToString();
                                        wss.Cells[i + 3, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                    }
                                }
                            }


                            for (int i = 0; i < dtSystem.Rows.Count; i++)
                            {
                                for (int j = 2; j < dtSystem.Columns.Count + 2; j++)
                                {
                                    if (i == 0)
                                    {
                                        wss.Cells[i + 2, j + 8].Value = dtSystem.Columns[j - 2].ColumnName.ToString();
                                        wss.Cells[i + 2, j + 8].Style.Font.Size = 12;
                                        wss.Cells[i + 2, j + 8].Style.Font.Bold = true;
                                        wss.Cells[i + 2, j + 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wss.Cells[i + 2, j + 8].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.CornflowerBlue);
                                        wss.Cells[i + 2, j + 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                        wss.Cells[i + 3, j + 8].Value = dtSystem.Rows[i][j - 2].ToString();
                                        wss.Cells[i + 3, j + 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                    }
                                    else
                                    {
                                        wss.Cells[i + 3, j + 8].Value = dtSystem.Rows[i][j - 2].ToString();
                                        wss.Cells[i + 3, j + 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);


                                    }
                                }
                            }


                            wss.Columns.AutoFit();
                            wss.Cells[1, 2, 1, 4].Merge = true;
                            wss.Cells[1, 2, 1, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            wss.Cells[1, 2].Value = "CARDHOLDER FILE";
                            wss.Cells[1, 2].Style.Font.Bold = true;

                            wss.Cells[1, 10, 1, 12].Merge = true;
                            wss.Cells[1, 10, 1, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            wss.Cells[1, 10].Value = "CPS SYSTEM";
                            wss.Cells[1, 10].Style.Font.Bold = true;
                            package.Save();

                        }


                        

                    }
                    else
                    {

                    }

                  

                }

                panel1.Enabled = true;
               // panel3.Visible = false;
                label1.Text = "";
                MessageBox.Show("RECON FILE SUCCESSFULLY GENERATED!!!", "SUCCESS");

            }
            catch (Exception ex)
            {
                panel1.Enabled = true;
               // panel3.Visible = false;
                label1.Text = "";
                MessageBox.Show(ex.Message +"\r\n\r\n"+ex.StackTrace, "ERROR");
            }
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbDiag = new FolderBrowserDialog();
            if (fbDiag.ShowDialog() == DialogResult.OK)
            {

                _outputDirectory = fbDiag.SelectedPath;
                textBox1.Text = _outputDirectory;
            }



        }

        private void btnRecon_Click(object sender, EventArgs e)
        {
            string reconType = "";
            bool validRecon = false;
            bool validPath = false;
            bool validHp = false;
            bool validMonth = false;


            try
            {
                if (cbxType.SelectedItem.ToString() == "" || cbxType.SelectedItem == null)
                {
                    MessageBox.Show("PLEASE SELECT THE RECON TYPE!!!", "ERROR");
                }
                else
                {
                    validRecon = true;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("PLEASE SELECT THE RECON TYPE!!!", "ERROR");
            }

            try
            {
                if (cbxHpCode.SelectedItem.ToString() == "" || cbxHpCode.SelectedItem == null)
                {
                    MessageBox.Show("PLEASE SELECT THE HEALTHPLAN TO RECON!!!", "ERROR");
                }
                else
                {
                    validHp = true;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("PLEASE SELECT THE HEALTHPLAN TO RECON!!!", "ERROR");
            }
            try
            {
                if (cbxMonth.SelectedItem.ToString() == "" || cbxMonth.SelectedItem == null)
                {
                    MessageBox.Show("PLEASE SELECT THE MONTH TO RECON!!!", "ERROR");
                }
                else
                {
                    validMonth = true;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("PLEASE SELECT THE MONTH TO RECON!!!", "ERROR");
            }
            if (_outputDirectory == "")
            {
                MessageBox.Show("PLEASE A VALID PATH TO SAVE THE RECON FILE TO!!!", "ERROR");
            }
            else
            {
                validPath = true;
            }
            try
            {
                if (validPath && validHp && validRecon && validMonth)
                {
                    reconType = cbxType.SelectedItem.ToString();
                    if (reconType.ToUpper() == "MAIN MEMBERS ONLY")
                    {
                        ReconType = true;
                    }
                    else
                    {
                        ReconType = false;
                    }

                    _healthplanName = cbxHpCode.SelectedItem.ToString().ToUpper();
                    _dateToRecon = cbxMonth.SelectedItem.ToString().ToUpper();
                    switch (_healthplanName)
                    {
                        case "AFFINITY HEALTH":
                            _fileId = "100";
                            _fileCount = "6";
                            _hpCode = "AFFI";
                            break;
                        case "AFFINITY DENTAL":
                            _fileId = "101";
                            _fileCount = "1";
                            _hpCode = "AFFD";
                            break;
                        case "ACUMEN":
                            _fileId = "179";
                            _fileCount = "1";
                            _hpCode = "ACUM";
                            break;
                        case "AGS":
                            _fileId = "124";
                            _fileCount = "1";
                            _hpCode = "AGS";
                            break;
                        case "ASTERIO":
                            _fileId = "199";
                            _fileCount = "1";
                            _hpCode = "ASTE";
                            break;
                        case "ESSENTIALMED":
                            _fileId = "286";
                            _fileCount = "1";
                            _hpCode = "ESSE";
                            break;
                        case "GETSAVVI":
                            _fileId = "115";
                            _fileCount = "1";
                            _hpCode = "GETS";
                            break;
                        case "MEDICALL":
                            _fileId = "144";
                            _fileCount = "1";
                            _hpCode = "MED";
                            break;
                        case "MOTOHEALTH":
                            _fileId = "287";
                            _fileCount = "1";
                            _hpCode = "MOTO";
                            break;
                        case "NBCRLFI":
                            _fileId = "102";
                            _fileCount = "8";
                            _hpCode = "NBCR";
                            break;
                        case "NBCNPCC":
                            _fileId = "136";
                            _fileCount = "6";
                            _hpCode = "NPSS";
                            break;
                        case "PROFMED":
                            _fileId = "94";
                            _fileCount = "1";
                            _hpCode = "PROF";
                            break;
                        case "SIZWE":
                            _fileId = "285";
                            _fileCount = "6";
                            _hpCode = "SIZW";
                            break;
                        case "HOSMED":
                            _fileId = "279";
                            _fileCount = "1";
                            _hpCode = "HOSM";
                            break;
                        case "WESMART":
                            _fileId = "288";
                            _fileCount = "1";
                            _hpCode = "WESM";
                            break;
                        default:
                            break;
                    }

                   

                    switch (_dateToRecon)
                    {
                        case "JANUARY":
                             _date = new DateTime(DateTime.Now.Year, 01, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        case "FEBRUARY":
                             _date = new DateTime(DateTime.Now.Year, 02, 01);
                            _dateToRecon =_date.ToString("yyyy/MM/dd");
                            break;
                        case "MARCH":
                             _date = new DateTime(DateTime.Now.Year, 03, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        case "APRIL":
                            _date = new DateTime(DateTime.Now.Year, 04, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        case "MAY":
                            _date = new DateTime(DateTime.Now.Year, 05, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        case "JUNE":
                            _date = new DateTime(DateTime.Now.Year, 06, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        case "JULY":
                            _date = new DateTime(DateTime.Now.Year, 07, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        case "AUGUST":
                            _date = new DateTime(DateTime.Now.Year, 08, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        case "SEPTEMBER":
                            _date = new DateTime(DateTime.Now.Year, 09, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        case "OCTOBER":
                            _date = new DateTime(DateTime.Now.Year, 10, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        case "NOVEMBER":
                            _date = new DateTime(DateTime.Now.Year, 11, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        case "DECEMBER":
                            _date = new DateTime(DateTime.Now.Year, 12, 01);
                            _dateToRecon = _date.ToString("yyyy/MM/dd");
                            break;
                        default:
                            break;
                    }
                    if (ReconType)
                    {
                        SqlConnection cn = new SqlConnection("Server=192.168.16.13;Database=DRC;User Id=sa;Password = galnetdata; Trusted_Connection=false ");
                        SqlCommand cmd = new SqlCommand();

                        if (cn.State != ConnectionState.Open)
                        {
                            cn.Open();
                        }

                        cmd.Connection = cn;
                        cmd.CommandTimeout = 6000;

                        cmd.Parameters.Add(new SqlParameter("@EDIROWID", _fileId));
                        cmd.Parameters.Add(new SqlParameter("@FILECOUNT", _fileCount));

                        DataTable dt = new DataTable();

                        cmd.CommandText = "EXEC dbo.EDI_MEMBERFILE_RELATION @EDIROWID, @FILECOUNT";

                        dt.Load(cmd.ExecuteReader());


                        _rlshipMain = dt.Rows[0][0].ToString();

                        if (_rlshipMain == "")
                        {
                            _rlshipMain = "00";
                        }

                    }

                    SuspendLayout();
                    StartProcessing(); 
                }
            }
            catch (Exception ex)
            {
                textBox1.Clear();
                cbxHpCode.Refresh();
                cbxType.Refresh();
                panel1.Enabled = true;
               // panel3.Visible = false;
                label1.Text = "";
                MessageBox.Show(ex.Message+"\r\n\r\n"+ex.StackTrace, "ERROR");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                throw new Exception("RECON WAS STOPPED BY USER!!");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "WARNING!!");
                textBox1.Clear();
                cbxHpCode.Refresh();
                cbxType.Refresh();
                panel1.Enabled = true;
               // panel3.Visible = false;
                label1.Text = "";
            } 
        }
    }
}
